export const data = {
  catalogs: [
    {
      id: 1,
      title: "Каталог",
      icon: "src/assets/icons/catalog.svg",
    },
    {
      id: 2,
      title: "Шины",
      icon: "src/assets/icons/tires.svg",
    },
    {
      id: 3,
      title: "Аккумуляторы",
      icon: "src/assets/icons/accumulators.svg",
    },
    {
      id: 4,
      title: "Автоэлектрика и автолампы",
      icon: "src/assets/icons/auto_electrican&car_lamps.svg",
    },
    {
      id: 5,
      title: "Щетки",
      icon: "src/assets/icons/brushes.svg",
    },
    {
      id: 6,
      title: "Фильтры",
      icon: "src/assets/icons/filters.svg",
    },
    {
      id: 7,
      title: "Жидкости и автохимия",
      icon: "src/assets/icons/liquids_and_auto_chemistry.svg",
    },
    {
      id: 8,
      title: "Смазочные материалы и трансмиссия",
      icon: "src/assets/icons/Lubricants_and_transmission.svg",
    },
    {
      id: 9,
      title: "Диски",
      icon: "src/assets/icons/disks.svg",
    },
  ],
};
