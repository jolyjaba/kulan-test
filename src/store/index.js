import Vue from "vue";
import Vuex from "vuex";
import { data } from "../../back-end";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    catalogs: [],
  },
  mutations: {
    SET_CATALOG_STATE(state, payload) {
      state.catalogs = payload.catalogs;
    },
  },
  actions: {
    getAllCatalogs(ctx) {
      ctx.commit("SET_CATALOG_STATE", data);
    },
  },
});
